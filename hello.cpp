#include <iostream>

int main(int argc, char** argv)
{
  if (argc == 2)
  {
    switch (*(argv[1]))
    {
      case 'f':
        std::cout << "Bonjour le monde" << std::endl;
        break;
      default:
        std::cout << "Unknown language" << std::endl;
        break;
    }
  }
  else
  {
    std::cout << "Hello world" << std::endl;
  }
}